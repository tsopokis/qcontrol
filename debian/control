Source: qcontrol
Maintainer: Ian Campbell <ijc@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper (>= 11~),
               libsystemd-dev,
               liblua5.1-0-dev,
               pkg-config
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/debian/qcontrol
Vcs-Git: https://salsa.debian.org/debian/qcontrol.git
Homepage: https://www.hellion.org.uk/qcontrol/
Rules-Requires-Root: no

Package: qcontrol
Architecture: armel armhf
Depends: ${shlibs:Depends},
         ${misc:Depends},
         udev
Pre-Depends: dpkg (>= 1.20.6)
Description: hardware control for QNAP Turbo Station devices
 Allows one to send commands to the microcontroller of supported devices,
 for example to change leds or sound a buzzer.
 .
 Depending on the device it can also monitor for example for button
 presses or temperature, with events triggering actions defined in the
 configuration file.
 .
 Currently supported devices are the QNAP TS-109, QNAP TS-11x,
 QNAP TS-12x, QNAP TS-209, QNAP HS-210, QNAP TS-21x, QNAP TS-22x,
 QNAP TS-409, QNAP TS-409U, QNAP TS-41x, QNAP TS-42x and Synology
 Diskstation and Rackstation.

Package: qcontrol-udeb
Architecture: armel armhf
Section: debian-installer
Depends: ${shlibs:Depends},
         ${misc:Depends},
         udev-udeb (>= 0.141-2),
         event-modules [armel]
Package-Type: udeb
Description: hardware control for QNAP Turbo Station devices
 Allows one to change status leds or sound the buzzer of supported devices.
Build-Profiles: <!noudeb>
